import { systemElevatorService } from '../services';
import { SystemElevatorsController } from './systemElevators.controller';

const systemElevatorsController = new SystemElevatorsController(systemElevatorService);

export {
    systemElevatorsController
}