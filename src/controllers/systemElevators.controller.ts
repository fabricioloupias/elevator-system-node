import { NextFunction, Request, Response } from 'express';
import { PickUpDTO } from '../dtos/pickUp.dto';
import { SystemElevatorsService } from '../services/systemElevators.service';
import { logger } from '../utils/logger';

export class SystemElevatorsController {
  private systemElevatorService: SystemElevatorsService;

  constructor(systemElevatorService: SystemElevatorsService) {
    this.systemElevatorService = systemElevatorService;
  }

  public pickUp = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const elevatorRequests = req.body.elevatorRequests as PickUpDTO[];
    try {
      elevatorRequests.forEach(r => {
        this.systemElevatorService.pickUp(r.requestedFloor, r.floorDestination);
      });

      this.systemElevatorService.run();
      logger.info(this.systemElevatorService.systemElevators);
      res.send(this.systemElevatorService.systemElevators);
    } catch (error) {
      logger.error(error);
      next(error)
    }
  };

  public getSystemElevator = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const elevatorSystem = this.systemElevatorService.systemElevators;

    res.send(elevatorSystem.toJson());
  };

  public run = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    this.systemElevatorService.run();
  };
}
