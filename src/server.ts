import 'dotenv/config';
import { systemElevatorsController } from './controllers';
import App from './app';
import { SystemElevatorsRoute } from './routes/systemElevators.route';
import validateEnv from './utils/validateEnv';

validateEnv();

const app = new App([new SystemElevatorsRoute(systemElevatorsController)]);

app.listen();
