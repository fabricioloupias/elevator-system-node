import { Elevator } from '../models/elevator.model';
import { ElevatorDirection } from '../models/elevatorDirection.enum';
import { ElevatorSystem } from '../models/elevatorSystem.model';

export class SystemElevatorsService {
  private _systemElevators: ElevatorSystem;

  constructor(systemElevators: ElevatorSystem) {
    this._systemElevators = systemElevators;
  }

  /**
   * Getter systemElevators
   * @return {ElevatorSystem}
   */
  public get systemElevators(): ElevatorSystem {
    return this._systemElevators;
  }

  public pickUp(requestedFloor: number, floorDestination: number): Elevator {
    return this.systemElevators.pickUpClosestElevator(requestedFloor, floorDestination);
  }

  public run() {
    this.systemElevators.run();
  }
}
