import { elevatorSystem } from "../models";
import { SystemElevatorsService } from "./systemElevators.service";

const systemElevatorService = new SystemElevatorsService(elevatorSystem);

export {
    systemElevatorService
}