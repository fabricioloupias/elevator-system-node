import { IsNumber } from 'class-validator';

export class PickUpDTO {
  @IsNumber()
  public floorDestination: number;
  @IsNumber()
  public requestedFloor: number;
}
