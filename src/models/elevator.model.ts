import { Collections, JIterator } from 'typescriptcollectionsframework';
import { TreeSet } from 'typescriptcollectionsframework';
import { ElevatorDirection } from './elevatorDirection.enum';

export class Elevator {
  private _id: number;
  private _currentFloor: number;
  private _direction: ElevatorDirection;
  private _totalFloors: number;
  private _upwardDestinationFloors: TreeSet<number> = new TreeSet<number>(Collections.getNumberComparator());
  private _downwardDestinationFloors: TreeSet<number> = new TreeSet<number>(Collections.getNumberComparator());
  private _downDestinationReversed: number[] = [];
  private _countFloors: number;

  constructor(totalFloors: number, id: number) {
    this.totalFloors = totalFloors;
    this.direction = ElevatorDirection.UP;
    this.currentFloor = 0;
    this.id = id;
    this.countFloors = 0;
  }

  /**
   * Getter id
   * @return {number}
   */
  public get id(): number {
    return this._id;
  }

  /**
   * Getter currentFloor
   * @return {number}
   */
  public get currentFloor(): number {
    return this._currentFloor;
  }

  /**
   * Getter downDestinationReversed
   * @return {number[] }
   */
  public get downDestinationReversed(): number[] {
    return this._downDestinationReversed;
  }

  /**
   * Getter countFloors
   * @return {number}
   */
  public get countFloors(): number {
    return this._countFloors;
  }

  /**
   * Setter downDestinationReversed
   * @param {number[] } value
   */
  public set downDestinationReversed(value: number[]) {
    this._downDestinationReversed = value;
  }

  /**
   * Setter countFloors
   * @param {number} value
   */
  public set countFloors(value: number) {
    this._countFloors = value;
  }

  /**
   * Getter direction
   * @return {ElevatorDirection}
   */
  public get direction(): ElevatorDirection {
    return this._direction;
  }

  /**
   * Getter totalFloors
   * @return {number}
   */
  public get totalFloors(): number {
    return this._totalFloors;
  }

  /**
   * Getter upwardDestinationFloors
   * @return {TreeSet<number>}
   */
  public get upwardDestinationFloors(): TreeSet<number> {
    return this._upwardDestinationFloors;
  }

  /**
   * Getter downwardDestinationFloors
   * @return {TreeSet<number>}
   */
  public get downwardDestinationFloors(): TreeSet<number> {
    return this._downwardDestinationFloors;
  }

  /**
   * Setter id
   * @param {number} value
   */
  public set id(value: number) {
    this._id = value;
  }

  /**
   * Setter currentFloor
   * @param {number} value
   */
  public set currentFloor(value: number) {
    this._currentFloor = value;
  }

  /**
   * Setter direction
   * @param {ElevatorDirection} value
   */
  public set direction(value: ElevatorDirection) {
    this._direction = value;
  }

  /**
   * Setter totalFloors
   * @param {number} value
   */
  public set totalFloors(value: number) {
    this._totalFloors = value;
  }

  /**
   * Setter upwardDestinationFloors
   * @param {TreeSet<number>} value
   */
  public set upwardDestinationFloors(value: TreeSet<number>) {
    this._upwardDestinationFloors = value;
  }

  /**
   * Setter downwardDestinationFloors
   * @param {TreeSet<number>} value
   */
  public set downwardDestinationFloors(value: TreeSet<number>) {
    this._downwardDestinationFloors = value;
  }

  /**
   * Runs elevator
   */
  public run() {
    this.countFloors = 0;
    for (const iter: JIterator<number> = this.upwardDestinationFloors.iterator(); iter.hasNext(); ) {
      const tmp: number = iter.next();
      if (tmp > this.currentFloor) {
        this.currentFloor = tmp;
      }
      this.countFloors++;
    }

    this.upwardDestinationFloors = new TreeSet<number>(Collections.getNumberComparator());
    this.direction = ElevatorDirection.DOWN;

    let lastDownDest = this.downwardDestinationFloors.last();

    while (lastDownDest != null) {
      if (lastDownDest < this.currentFloor) {
        this.currentFloor = lastDownDest;
      }
      this.downwardDestinationFloors.remove(lastDownDest);
      this.countFloors++;
      lastDownDest = this.downwardDestinationFloors.last();
    }

    this.downwardDestinationFloors = new TreeSet<number>(Collections.getNumberComparator());
    this.direction = ElevatorDirection.UP;
  }

  public addToUpQueue(floor: number, isDestinationFloor: boolean = false) {
    if (!this.upwardDestinationFloors.contains(floor) && (this.currentFloor != floor || isDestinationFloor)) {
      this.upwardDestinationFloors.add(floor);
    }
  }

  public addToDownQueue(floor: number, isDestinationFloor: boolean = false) {
    if (!this.downwardDestinationFloors.contains(floor) && (this.currentFloor != floor || isDestinationFloor)) {
      this.downwardDestinationFloors.add(floor);
    }
  }

  public toJson() {
    return {
      id: this.id,
      currentFloor: this.currentFloor,
      direction: this.direction.valueOf(),
      upwardDestinationFloors: this.upwardDestinationFloors,
      downwardDestinationFloors: this.downwardDestinationFloors,
      countFloors: this.countFloors
    };
  }
}
