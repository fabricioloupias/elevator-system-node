import { Elevator } from './elevator.model';
import { ElevatorDirection } from './elevatorDirection.enum';

export class ElevatorSystem {
  private _elevators: Elevator[] = [];
  private _totalElevators: number;
  private _maxFloors: number;
  private _nameSpace: string;
  private _totalFloorsByRequest: number;

  constructor(totalElevators: number, maxFloors: number, nameSpace: string) {
    this._totalElevators = totalElevators;
    this.maxFloors = maxFloors;
    this.nameSpace = nameSpace;
    this.totalFloorsByRequest = 0;
    this.initElevators();
  }

  /**
   * Getter totalElevators
   * @return {number}
   */
  public get totalElevators(): number {
    return this._totalElevators;
  }

  /**
   * Getter maxFloors
   * @return {number}
   */
  public get maxFloors(): number {
    return this._maxFloors;
  }

  /**
   * Getter nameSpace
   * @return {string}
   */
  public get nameSpace(): string {
    return this._nameSpace;
  }

  /**
   * Getter totalFloorsByRequest
   * @return {number}
   */
  public get totalFloorsByRequest(): number {
    return this._totalFloorsByRequest;
  }

  /**
   * Setter totalFloorsByRequest
   * @param {number} value
   */
  public set totalFloorsByRequest(value: number) {
    this._totalFloorsByRequest = value;
  }

  /**
   * Setter totalElevators
   * @param {number} value
   */
  public set totalElevators(value: number) {
    this.totalElevators = value;
  }

  /**
   * Setter maxFloors
   * @param {number} value
   */
  public set maxFloors(value: number) {
    this._maxFloors = value;
  }

  /**
   * Setter nameSpace
   * @param {string} value
   */
  public set nameSpace(value: string) {
    this._nameSpace = value;
  }

  /**
   * Getter elevators
   * @return {Elevator[] }
   */
  public get elevators(): Elevator[] {
    return this._elevators;
  }

  /**
   * Setter elevators
   * @param {Elevator[] } value
   */
  public set elevators(value: Elevator[]) {
    this._elevators = value;
  }

  /**
   * Inits elevators
   */
  private initElevators() {
    for (let index = 0; index < this.totalElevators; index++) {
      const element = new Elevator(this.maxFloors, index);
      this.elevators.push(element);
    }
  }

  /**
   * Find the closest elevator, taking into account the current floor where
   * the elevator is located and the floor of origin
   * @param requestedFloor floor of origin
   * @param floorDestination floor of destination
   * @returns closest elevator
   */

  public pickUpClosestElevator(requestedFloor: number, floorDestination: number): Elevator {
    if(requestedFloor > this.maxFloors){
      throw new Error("Floor request can´t be greater than the total floors")
    }

    if((floorDestination - 1) > this.maxFloors){
      throw new Error("Floor destination can´t be greater than the total floors")
    }

    let closestElevator: Elevator;
    let elevatorsUpDirection = this.getElevatorsByDirection(ElevatorDirection.UP);
    let elevatorsDownDirection = this.getElevatorsByDirection(ElevatorDirection.DOWN);

    if (requestedFloor < floorDestination && elevatorsUpDirection.length > 0) {
      const index = elevatorsUpDirection.reduce(function (r, a, i, aa) {
        return i && Math.abs(aa[r].currentFloor - requestedFloor) < Math.abs(a.currentFloor - requestedFloor) ? r : i;
      }, -1);

      closestElevator = elevatorsUpDirection[index];
    } else if (requestedFloor > floorDestination && elevatorsDownDirection.length > 0) {
      const index = this.elevators.reduce(function (r, a, i, aa) {
        return i && Math.abs(aa[r].currentFloor - requestedFloor) < Math.abs(a.currentFloor - requestedFloor) ? r : i;
      }, -1);

      closestElevator = elevatorsDownDirection[index];
    } else {
      closestElevator = this.elevators[0];
    }

    // Add requested floor to queue
    if (closestElevator.currentFloor < requestedFloor) {
      closestElevator.addToUpQueue(requestedFloor);
    } else {
      closestElevator.addToDownQueue(requestedFloor);
    }

    // Add floor destination to queue
    if (requestedFloor > floorDestination) {
      closestElevator.addToDownQueue(floorDestination, true);
    } else {
      closestElevator.addToUpQueue(floorDestination, true);
    }

    return closestElevator;
  }

  /**
   * Go through all the elevators, run their cycle and count the number of floors traveled
   */
  public run() {
    this.totalFloorsByRequest = 0;
    this.elevators.forEach(e => {
      e.run();
      this.totalFloorsByRequest += e.countFloors;
    });
  }

  /**
   * Gets elevators by direction
   * @param direction Elevator current direction
   * @returns  
   */
  public getElevatorsByDirection(direction: ElevatorDirection): Elevator[] {
    return this.elevators.filter(e => e.direction == direction);
  }

  public toJson(){
    return {
      totalElevators: this.totalElevators,
      maxFloors: this.maxFloors,
      nameSpace: this.nameSpace,
      totalFloorsByRequest: this.totalFloorsByRequest,
      elevators: this.elevators.map(e => e.toJson())
    }
  }
}
