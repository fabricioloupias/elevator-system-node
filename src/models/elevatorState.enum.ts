export enum ElevatorState {
  GOING_UP,
  STILL, // IDLE
  GOING_DOWN,
}
