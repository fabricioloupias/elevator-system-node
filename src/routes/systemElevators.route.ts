import { Router } from 'express';
import { SystemElevatorsController } from '../controllers/systemElevators.controller';
import Route from '../interfaces/routes.interface';

export class SystemElevatorsRoute implements Route {
  public path = '/system-elevators';
  public router = Router();
  public systemElevatorsController: SystemElevatorsController;

  constructor(systemElevatorsController: SystemElevatorsController) {
    this.systemElevatorsController = systemElevatorsController;
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}`, this.systemElevatorsController.pickUp);
    this.router.get(`${this.path}`, this.systemElevatorsController.getSystemElevator);
    this.router.get(`${this.path}/run`, this.systemElevatorsController.run)
  }
}